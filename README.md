# CLRS Torch

The original [CLRS](https://github.com/google-deepmind/clrs) library is built using Google's machine learning framework jax.
While jax is a good framework, the ecosystem that surrounds it

## Getting started

CLRS Torch can be installed with pip, directly from GitLab:

```shell
pip install git+https://gitlab.com/rhododendrox/clrs-torch.git
```

You may prefer to install it in a virtual environment to avoid dependency conflicts :

```shell
python3 -m venv clrs_env
source clrs_env/bin/activate
pip install git+https://gitlab.com/rhododendrox/clrs-torch.git
```

Once installed you can run the example baseline model:

```shell
python3 -m clrs_torch.examples.run
```

## Adapting existing code

Existing code using the original CLRS library can be easily modified to work with CLRS Torch.

### Replace all `clrs` occurrences with `clrs_torch`

The first step is of course to use the new library.
CLRS Torch reexports all CLRS modules, while overriding some of them with their new PyTorch based implementation.
This way it acts almost as a drop in replacement.

<table>
<tr>
<th>CLRS</th>
<th>CLRS Torch</th>
</tr>
<tr>
<td>

```python
import clrs

...

factory = clrs.get_processor_factory(
  kind='mpnn',
  use_ln=True
)

model = clrs.models.BaselineModel(
  spec=spec,
  dummy_trajectory=next(sampler),
  processor_factory=factory,
)
```

</td>
<td>

```python
import clrs_torch

...

factory = clrs_torch.get_processor_factory(
  kind='mpnn',
  use_ln=True
)

model = clrs_torch.models.BaselineModel(
  spec=spec,
  dummy_trajectory=next(sampler),
  processor_factory=factory,
)
```

</td>
</tr>
</table>

### Convert the training data into PyTorch tensors

All datapoints should be converted before being used for training.
An easy way to achieve this is using the function `torchify_sampler`.
This function takes a sampler as an argument and returns a new sampler, which takes care to apply all required conversions.

<table>
<tr>
<th>CLRS</th>
<th>CLRS Torch</th>
</tr>
<tr>
<td>

```python
sampler, num_samples, spec = clrs.create_dataset(
  folder='./dataset',
  algorithm='insertion_sort',
  batch_size=32,
  split='train'
)
sampler = sampler.as_numpy_iterator()
```

</td>
<td>

```python
sampler, num_samples, spec = clrs.create_dataset(
  folder='./dataset',
  algorithm='insertion_sort',
  batch_size=32,
  split='train'
)
sampler = sampler.as_numpy_iterator()

sampler = clrs_torch.torchify_sampler(sampler)
```

</td>
</tr>
</table>

### Remove all explicit parameters initializations

Following PyTorch, CLRS Torch automatically initializes all model parameter when the constructor is called.
Therefore is no longer required to call the method `init` for baseline models.

<table>
<tr>
<th>CLRS</th>
<th>CLRS Torch</th>
</tr>
<tr>
<td>

```python
model = clrs.models.BaselineModel(
  spec=spec,
  dummy_trajectory=[next(sampler)],
  processor_factory=factory,
)

model.init([next(sampler)], 42)
```

</td>
<td>

```python
model = clrs.models.BaselineModel(
  spec=spec,
  dummy_trajectory=[next(sampler)],
  processor_factory=factory,
)
```

</td>
</tr>
</table>

### Ensure reproducibility through `torch.random.manual_seed`

PyTorch doesn't require the user to explicitly handle the PRNG state the same way JAX does.
Therefore rng keys are no longer needed and setting an initial seed through `torch.random.manual_seed` should suffice to ensure reproducibility.

<table>
<tr>
<th>CLRS</th>
<th>CLRS Torch</th>
</tr>
<tr>
<td>

```python
rng_key = jax.random.PRNGKey(42)

...

rng_key, new_rng_key = jax.random.split(rng_key)
loss = model.feedback(rng_key, feedback, 0)

rng_key = new_rng_key
```

</td>
<td>

```python
torch.random.manual_seed(42)

...

loss = model.feedback(feedback, 0)
```

</td>
</tr>
</table>
