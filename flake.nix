{
  description = "Clrs torch development flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
  };

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
          "cudatoolkit"
          "nvidia-x11"
        ];
      };
      lib = nixpkgs.lib;
    in {
      devShells."${system}".default = (pkgs.buildFHSUserEnv {
        name = "pipenv";
        targetPkgs = pkgs: (with pkgs; [
          cudatoolkit
          gcc
          linuxPackages.nvidia_x11
          python310
          python310Packages.pip
          python310Packages.virtualenv
          zlib
        ]);
        profile = ''
          export CUDA_PATH=${pkgs.cudatoolkit}
        '';
      }).env;
    };
}
