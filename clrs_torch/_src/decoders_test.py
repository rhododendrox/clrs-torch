# Copyright 2022 DeepMind Technologies Limited. All Rights Reserved.
# Modifications copyright 2023 Samuele Boldrini
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Unit tests for `decoders.py`."""

import torch
import torch.testing as testing
from absl.testing import absltest

from clrs_torch._src import decoders


class DecodersTest(absltest.TestCase):

    def test_log_sinkhorn(self):
        torch.manual_seed(42)

        x = torch.normal(mean=0, std=1, size=(10, 10))
        y = torch.exp(
            decoders.log_sinkhorn(x,
                                  steps=10,
                                  temperature=1.0,
                                  zero_diagonal=False,
                                  add_noise=False))
        y_1 = torch.sum(y, dim=-1)
        testing.assert_close(y_1, torch.full_like(y_1, 1.), rtol=1e-06, atol=1e-4)
        y_2 = torch.sum(y, dim=-2)
        testing.assert_close(y_2, torch.full_like(y_2, 1.), rtol=1e-06, atol=1e-4)

    def test_log_sinkhorn_zero_diagonal(self):
        torch.manual_seed(42)

        x = torch.normal(mean=0, std=1, size=(10, 10))
        y = torch.exp(
            decoders.log_sinkhorn(x,
                                  steps=10,
                                  temperature=1.0,
                                  zero_diagonal=True,
                                  add_noise=False))
        y_1 = torch.sum(y, dim=-1)
        testing.assert_close(y_1, torch.full_like(y_1, 1.), rtol=1e-06, atol=1e-4)
        y_2 = torch.sum(y, dim=-2)
        testing.assert_close(y_2, torch.full_like(y_2, 1.), rtol=1e-06, atol=1e-4)
        y_d = torch.sum(y.diagonal())
        testing.assert_close(y_d, torch.full_like(y_d, 0.), rtol=1e-06, atol=1e-4)


if __name__ == '__main__':
    absltest.main()
