# Copyright 2021 DeepMind Technologies Limited. All Rights Reserved.
# Modifications copyright 2023 Samuele Boldrini
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Probing utilities.

The dataflow for an algorithm is represented by `(stage, loc, type, data)`
"probes" that are valid under that algorithm's spec (see `specs.py`).

"""

import attr
import jax
import numpy as np
import tensorflow as tf
import torch
from clrs._src import specs

_Location = specs.Location
_Stage = specs.Stage
_Type = specs.Type
_OutputClass = specs.OutputClass

_Array = torch.Tensor


def _convert_to_str(element):
    if (isinstance(element, tf.Tensor) or isinstance(element, torch.Tensor)):
        return element.numpy().decode('utf-8')
    elif isinstance(element, (np.ndarray, bytes)):
        return element.decode('utf-8')
    else:
        return element


# First anotation makes this object jax.jit/pmap friendly,
# second one makes this tf.data.Datasets friendly.
@jax.tree_util.register_pytree_node_class
@attr.define
class DataPoint:
    """Describes a data point."""

    _name: str
    _location: str
    _type_: str
    data: _Array

    @property
    def name(self):
        return _convert_to_str(self._name)

    @property
    def location(self):
        return _convert_to_str(self._location)

    @property
    def type_(self):
        return _convert_to_str(self._type_)

    def __repr__(self):
        s = f'DataPoint(name="{self.name}",\tlocation={self.location},\t'
        return s + f'type={self.type_},\tdata=Array{self.data.shape})'

    def tree_flatten(self):
        data = (self.data, )
        meta = (self.name, self.location, self.type_)
        return data, meta

    @classmethod
    def tree_unflatten(cls, meta, data):
        name, location, type_ = meta
        subdata, = data
        return DataPoint(name, location, type_, subdata)


def torchify_datapoint(dp):
    """Converts datapoint's `data` attribute to torch.Tensor'"""

    if isinstance(dp.data, jax.Array):
        transform_fn = lambda x: torch.tensor(np.asarray(x), dtype=torch.float32)
    elif isinstance(dp.data, np.ndarray):
        transform_fn = lambda x: torch.tensor(x, dtype=torch.float32)
    else:
        raise TypeError(f'dp.data has an unexpected type ({type(dp.data)})')

    return jax.tree_util.tree_map(transform_fn, dp)
