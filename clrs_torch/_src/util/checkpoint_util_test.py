# Copyright 2023 Samuele Boldrini
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Unit tests for `checkpoint_util.py`."""

from typing import Generator

import chex
import jax
import numpy as np
from absl.testing import absltest
from clrs._src import baselines as jax_baselines
from clrs._src import processors as jax_processors
from clrs._src import samplers
from clrs._src import specs

from clrs_torch._src import baselines as torch_baselines
from clrs_torch._src import processors as torch_processors
from clrs_torch._src.samplers import torchify_feedback
from clrs_torch._src.util.checkpoint_util import torchify_checkpoint_params


def _make_sampler(algo: str, length: int) -> samplers.Sampler:
    sampler, _ = samplers.build_sampler(
        algo,
        seed=samplers.CLRS30['val']['seed'],
        num_samples=samplers.CLRS30['val']['num_samples'],
        length=length,
    )
    return sampler


def _make_iterable_sampler(
        algo: str, batch_size: int,
        length: int) -> Generator[samplers.Feedback, None, None]:
    sampler = _make_sampler(algo, length)
    while True:
        yield sampler.next(batch_size)


class CheckpointUtilTest(absltest.TestCase):

    def test_params_conversion(self):
        """Test JAX to Torch parameters conversion."""

        batch_size = 4
        length = 8
        algo = 'bfs'
        spec = specs.SPECS[algo]
        rng_key = jax.random.PRNGKey(42)

        sampler = _make_iterable_sampler(algo, batch_size, length)

        jax_batch = next(sampler)
        torch_batch = torchify_feedback(jax_batch)

        with chex.fake_jit():

            common_args = dict(hidden_dim=8, learning_rate=0.01,
                               decode_hints=True, encode_hints=True)
            jax_processor_factory = jax_processors.get_processor_factory(
                'mpnn', use_ln=False, nb_triplet_fts=0)
            torch_processor_factory = torch_processors.get_processor_factory(
                'mpnn', use_ln=False, nb_triplet_fts=0)

            jax_model = jax_baselines.BaselineModel(
                spec, dummy_trajectory=jax_batch,
                processor_factory=jax_processor_factory, **common_args)
            jax_model.init(jax_batch.features, seed=42)

            torch_model = torch_baselines.BaselineModel(
                spec, dummy_trajectory=torch_batch,
                processor_factory=torch_processor_factory, **common_args)
            torch_model.net.load_state_dict(
                torchify_checkpoint_params(jax_model.params))

            jax_out = jax_model.predict(rng_key, jax_batch.features)
            torch_out = torch_model.predict(torch_batch.features)

            jax_loss = jax_model.feedback(rng_key, jax_batch)
            torch_loss = torch_model.feedback(torch_batch)

        np.testing.assert_allclose(jax_loss, torch_loss, rtol=1e-4)

        flat_jax_out, _ = jax.tree_util.tree_flatten(jax_out)
        flat_torch_out, _ = jax.tree_util.tree_flatten(torch_out)

        jax.tree_util.tree_map(
            lambda j, t: np.testing.assert_allclose(j, t.detach(), rtol=1e-4),
            flat_jax_out, flat_torch_out)


if __name__ == '__main__':
    absltest.main()
