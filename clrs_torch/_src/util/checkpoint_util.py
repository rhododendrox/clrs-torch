# Copyright 2023 Samuele Boldrini
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Utilities for working with checkpoints."""

import re
from typing import Dict, List, Tuple

import jax
import numpy as np
import torch
from clrs._src.processors import PROCESSOR_TAG


_Array = jax.Array
_Tensor = torch.Tensor

# This datastructure maps processor layer numbers (e.g. linear_1)
# to the respective torch parameter name, for all processors.
PROCESSOR_LAYER_MAPS = {
    'gat_aggr': [
        'm', 'skip',
        'a_1', 'a_2',
        'a_e', 'a_g',
    ],
    'gatv2_aggr': [
        'm', 'skip',
        'w_1', 'w_2',
        'w_e', 'w_g',
    ],
    'mpnn_aggr': {
        'base': [
            'm_1', 'm_2',
            'm_e', 'm_g',
            'o1', 'o2',
        ],
        'triplets': [
            't_1', 't_2', 't_3',
            't_e_1', 't_e_2', 't_e_3',
            't_g', 'o3',
        ],
        'gates': [
            'gate1',
            'gate2',
            'gate3',
        ],
    },
    'memnet': [
        'intermediate_linear',
        'output_linear',
        'predict_linear'
    ],
}


def _torchify_dict_key(
        key: str,
        processor_maps: List[str]
) -> Tuple[Tuple[str, str], ...]:
    is_encoder = re.compile('algo_([0-9]+)_(.*)_enc_linear(_[1-9][0-9]*)?$')
    is_decoder = re.compile('algo_([0-9]+)_(.*)_dec_linear(_[1-9][0-9]*)?$')

    if PROCESSOR_TAG in key:
        if 'layer_norm' in key:
            key_maps = (
                ('scale', 'processor.ln.weight'),
                ('offset', 'processor.ln.bias'),
            )
        elif 'mlp' in key and (match := re.search('linear_([0-9]+)$', key)):
            stage = match.group(1)
            key_maps = (
                ('w', 'processor.mlp.layers.{}.weight'.format(stage)),
                ('b', 'processor.mlp.layers.{}.bias'.format(stage)),
            )
        elif 'linear' in key and (match := re.search('linear(_[1-9][0-9]*)?$', key)):
            processor_type = re.search(f'/(.*)_{PROCESSOR_TAG}/', key).group(1)
            stage = match.group(1)
            idx = 0 if stage is None else int(stage[1:])

            if processor_type == 'memnet':
                key_maps = (
                    ('w', 'processor.{}.weight'.format(processor_maps[idx])),
                )
            else:
                key_maps = (
                    ('w', 'processor.{}.weight'.format(processor_maps[idx])),
                    ('b', 'processor.{}.bias'.format(processor_maps[idx])),
                )
        elif key.endswith(PROCESSOR_TAG):
            key_maps = (
                ('query_biases', 'processor.query_biases'),
                ('stories_biases', 'processor.stories_biases'),
                ('memory_contents', 'processor.memory_biases'),
                ('output_biases', 'processor.output_biases'),
            )
        else:
            raise ValueError(f'Unknown parameter name {key}')
    elif (match := is_encoder.search(key)):
        algo, name, idx = match.groups()
        key_maps = (
            ('w', 'encoders.{}.{}.{}.weight'.format(algo, name, '0' if idx is None else idx[1:])),
            ('b', 'encoders.{}.{}.{}.bias'.format(algo, name, '0' if idx is None else idx[1:])),
        )
    elif (match := is_decoder.search(key)):
        algo, name, idx = match.groups()
        key_maps = (
            ('w', 'decoders.{}.{}.{}.weight'.format(algo, name, '0' if idx is None else idx[1:])),
            ('b', 'decoders.{}.{}.{}.bias'.format(algo, name, '0' if idx is None else idx[1:])),
        )
    elif 'processor_lstm' in key:
        key_maps = (
            ('w', 'lstm.gate.weight'),
            ('b', 'lstm.gate.bias'),
        )
    else:
        raise ValueError(f'Unknown parameter name {key}')

    return key_maps


def torchify_checkpoint_params(checkpoint: Dict[str, _Array]) -> Dict[str, _Tensor]:
    is_processor = re.compile(f'/(.*)_{PROCESSOR_TAG}(/~_apply)?/linear(_[1-9][0-9]*)?$')
    processor_params = list(filter(lambda k: is_processor.search(k),
                                   checkpoint.keys()))
    processor_type = is_processor.search(processor_params[0]).group(1)

    processor_maps = PROCESSOR_LAYER_MAPS[processor_type]
    if processor_type == 'mpnn_aggr':
        # Using the number of linear layer composing the processor
        # we can deduce if it uses triplets and/or gates.
        processor_maps = PROCESSOR_LAYER_MAPS['mpnn_aggr']['base']
        if len(processor_params) != len(processor_maps):
            if len(processor_params) == 14:
                processor_maps += PROCESSOR_LAYER_MAPS['mpnn_aggr']['triplets']
            elif len(processor_params) == 9:
                processor_maps += PROCESSOR_LAYER_MAPS['mpnn_aggr']['gates']
            elif len(processor_params) == 17:
                processor_maps += PROCESSOR_LAYER_MAPS['mpnn_aggr']['triplets']
                processor_maps += PROCESSOR_LAYER_MAPS['mpnn_aggr']['gates']
            else:
                raise ValueError('Unexpected number of parameters in processor state')
    elif processor_type == 'gatv2_aggr':
        assert len(processor_params) > len(processor_maps)
        # Add the right number of attention heads
        for i in range(len(processor_params) - len(processor_maps)):
            processor_maps.append(f'a_heads.{i}')

    torch_state = {}
    for k, v in checkpoint.items():
        for (old, new) in _torchify_dict_key(k, processor_maps):
            if (old in {'query_biases', 'stories_biases',
                        'memory_contents', 'output_biases'}):
                torch_state[new] = torch.tensor(np.asarray(v[old]))
            else:
                torch_state[new] = torch.tensor(np.asarray(v[old]).T)

    return torch_state
