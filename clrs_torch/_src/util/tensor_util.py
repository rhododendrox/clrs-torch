# Copyright 2023 Samuele Boldrini
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Utilities for working with tensors."""

from typing import Sequence

import torch
from jax._src.util import canonicalize_axis as canonicalize_dims


_Tensor = torch.Tensor


def expand_dims(tensor: _Tensor, dims: Sequence[int]) -> _Tensor:
    """Insert any number of size 1 dimensions into a tensor."""
    if len(set(dims)) != len(dims):
        raise ValueError(f'repeated dimensions in util.expand_dims: {dims}')

    ndim_out = tensor.ndim + len(dims)
    dims = [canonicalize_dims(i, ndim_out) for i in dims]

    if len(set(dims)) != len(dims):  # check again after canonicalizing
        raise ValueError(f'repeated dimensions in util.expand_dims: {dims}')

    dims_set = frozenset(dims)
    for i in sorted(dims_set):
        tensor = torch.unsqueeze(tensor, i)

    return tensor
