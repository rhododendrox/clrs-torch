# Copyright 2021 DeepMind Technologies Limited. All Rights Reserved.
# Modifications copyright 2023 Samuele Boldrini
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Torch implementation of baseline processor networks."""

import abc
import functools
from typing import Any, Callable, List, Optional, Tuple

import torch
import torch.nn.functional as F
from torch.nn import ModuleList, Parameter

import clrs_torch._src.nn as nn
import clrs_torch._src.util as util
from clrs_torch._src import experimental


_Array = torch.Tensor
_Fn = Callable[..., Any]
BIG_NUMBER = 1e6


class Processor(torch.nn.Module):
    """Processor abstract base class."""

    def __init__(self):
        super(Processor, self).__init__()

    @abc.abstractmethod
    def forward(
        self,
        node_fts: _Array,
        edge_fts: _Array,
        graph_fts: _Array,
        adj_mat: _Array,
        hidden: _Array,
        **kwargs,
    ) -> Tuple[_Array, Optional[_Array]]:
        """Processor inference step.

    Args:
      node_fts: Node features.
      edge_fts: Edge features.
      graph_fts: Graph features.
      adj_mat: Graph adjacency matrix.
      hidden: Hidden features.
      **kwargs: Extra kwargs.

    Returns:
      Output of processor inference step as a 2-tuple of (node, edge)
      embeddings. The edge embeddings can be None.
    """
        pass

    @property
    def inf_bias(self):
        return False

    @property
    def inf_bias_edge(self):
        return False


class GAT(Processor):
    """Graph Attention Network (Velickovic et al., ICLR 2018)."""

    def __init__(
        self,
        hidden_size: int,
        nb_heads: int,
        activation: Optional[_Fn] = F.relu,
        residual: bool = True,
        use_ln: bool = False,
    ):
        super(GAT, self).__init__()
        self.hidden_size = hidden_size
        self.nb_heads = nb_heads
        if hidden_size % nb_heads != 0:
            raise ValueError(
                'The number of attention heads must divide the width!')
        self.head_size = hidden_size // nb_heads
        self.activation = activation
        self.residual = residual
        self.use_ln = use_ln

        self.m = nn.Linear(2 * self.hidden_size, self.hidden_size)
        self.skip = nn.Linear(2 * self.hidden_size, self.hidden_size)

        self.a_1 = nn.Linear(2 * self.hidden_size, self.nb_heads)
        self.a_2 = nn.Linear(2 * self.hidden_size, self.nb_heads)
        self.a_e = nn.Linear(self.hidden_size, self.nb_heads)
        self.a_g = nn.Linear(self.hidden_size, self.nb_heads)

        if self.use_ln:
            self.ln = torch.nn.LayerNorm(normalized_shape=self.hidden_size)

    def forward(  # pytype: disable=signature-mismatch  # numpy-scalars
        self,
        node_fts: _Array,
        edge_fts: _Array,
        graph_fts: _Array,
        adj_mat: _Array,
        hidden: _Array,
        **unused_kwargs,
    ) -> _Array:
        """GAT inference step."""

        b, n, _ = node_fts.shape
        assert edge_fts.shape[:-1] == (b, n, n)
        assert graph_fts.shape[:-1] == (b, )
        assert adj_mat.shape == (b, n, n)

        z = torch.concat([node_fts, hidden], dim=-1)

        bias_mat = (adj_mat - 1.0) * 1e9
        bias_mat = torch.tile(bias_mat[..., None],
                              (1, 1, 1, self.nb_heads))  # [B, N, N, H]
        bias_mat = torch.permute(bias_mat, (0, 3, 1, 2))  # [B, H, N, N]

        values = self.m(z)  # [B, N, H*F]
        values = torch.reshape(values, values.shape[:-1] +
                               (self.nb_heads, self.head_size))  # [B, N, H, F]
        values = torch.permute(values, (0, 2, 1, 3))  # [B, H, N, F]

        att_1 = torch.unsqueeze(self.a_1(z), dim=-1)
        att_2 = torch.unsqueeze(self.a_2(z), dim=-1)
        att_e = self.a_e(edge_fts)
        att_g = torch.unsqueeze(self.a_g(graph_fts), dim=-1)

        logits = (
            torch.permute(att_1, (0, 2, 1, 3)) +  # + [B, H, N, 1]
            torch.permute(att_2, (0, 2, 3, 1)) +  # + [B, H, 1, N]
            torch.permute(att_e, (0, 3, 1, 2)) +  # + [B, H, N, N]
            torch.unsqueeze(att_g, dim=-1)  # + [B, H, 1, 1]
        )  # = [B, H, N, N]
        coefs = F.softmax(F.leaky_relu(logits) + bias_mat, dim=-1)
        ret = torch.matmul(coefs, values)  # [B, H, N, F]
        ret = torch.permute(ret, (0, 2, 1, 3))  # [B, N, H, F]
        ret = torch.reshape(ret,
                            ret.shape[:-2] + (self.hidden_size,))  # [B, N, H*F]

        if self.residual:
            ret += self.skip(z)

        if self.activation is not None:
            ret = self.activation(ret)

        if self.use_ln:
            ret = self.ln(ret)

        return ret, None  # pytype: disable=bad-return-type  # numpy-scalars


class GATFull(GAT):
    """Graph Attention Network with full adjacency matrix."""

    def forward(self, node_fts: _Array, edge_fts: _Array, graph_fts: _Array,
                adj_mat: _Array, hidden: _Array, **unused_kwargs) -> _Array:
        adj_mat = torch.ones_like(adj_mat)
        return super().forward(node_fts, edge_fts, graph_fts, adj_mat, hidden)


class GATv2(Processor):
    """Graph Attention Network v2 (Brody et al., ICLR 2022)."""

    def __init__(
        self,
        hidden_size: int,
        nb_heads: int,
        mid_size: Optional[int] = None,
        activation: Optional[_Fn] = F.relu,
        residual: bool = True,
        use_ln: bool = False,
    ):
        super(GATv2, self).__init__()
        if mid_size is None:
            self.mid_size = hidden_size
        else:
            self.mid_size = mid_size
        self.hidden_size = hidden_size
        self.nb_heads = nb_heads
        if hidden_size % nb_heads != 0:
            raise ValueError(
                'The number of attention heads must divide the width!')
        self.head_size = hidden_size // nb_heads
        if self.mid_size % nb_heads != 0:
            raise ValueError(
                'The number of attention heads must divide the message!')
        self.mid_head_size = self.mid_size // nb_heads
        self.activation = activation
        self.residual = residual
        self.use_ln = use_ln

        self.m = nn.Linear(2 * self.hidden_size, self.hidden_size)
        self.skip = nn.Linear(2 * self.hidden_size, self.hidden_size)

        self.w_1 = nn.Linear(2 * self.hidden_size, self.mid_size)
        self.w_2 = nn.Linear(2 * self.hidden_size, self.mid_size)
        self.w_e = nn.Linear(self.hidden_size, self.mid_size)
        self.w_g = nn.Linear(self.hidden_size, self.mid_size)

        self.a_heads = ModuleList()
        for _ in range(self.nb_heads):
            self.a_heads.append(nn.Linear(self.mid_head_size, 1))

        if self.use_ln:
            self.ln = torch.nn.LayerNorm(normalized_shape=self.hidden_size)

    def forward(  # pytype: disable=signature-mismatch  # numpy-scalars
        self,
        node_fts: _Array,
        edge_fts: _Array,
        graph_fts: _Array,
        adj_mat: _Array,
        hidden: _Array,
        **unused_kwargs,
    ) -> _Array:
        """GATv2 inference step."""

        b, n, _ = node_fts.shape
        assert edge_fts.shape[:-1] == (b, n, n)
        assert graph_fts.shape[:-1] == (b, )
        assert adj_mat.shape == (b, n, n)

        z = torch.concat([node_fts, hidden], dim=-1)

        bias_mat = (adj_mat - 1.0) * 1e9
        bias_mat = torch.tile(bias_mat[..., None],
                              (1, 1, 1, self.nb_heads))  # [B, N, N, H]
        bias_mat = torch.permute(bias_mat, (0, 3, 1, 2))  # [B, H, N, N]

        values = self.m(z)  # [B, N, H*F]
        values = torch.reshape(values, values.shape[:-1] +
                               (self.nb_heads, self.head_size))  # [B, N, H, F]
        values = torch.permute(values, (0, 2, 1, 3))  # [B, H, N, F]

        pre_att_1 = self.w_1(z)
        pre_att_2 = self.w_2(z)
        pre_att_e = self.w_e(edge_fts)
        pre_att_g = self.w_g(graph_fts)

        pre_att = (
            torch.unsqueeze(pre_att_1, dim=1) +       # + [B, 1, N, H*F]
            torch.unsqueeze(pre_att_2, dim=2) +       # + [B, N, 1, H*F]
            pre_att_e +                               # + [B, N, N, H*F]
            util.expand_dims(pre_att_g, dims=(1, 2))  # + [B, 1, 1, H*F]
        )                                             # = [B, N, N, H*F]

        pre_att = torch.reshape(
            pre_att, pre_att.shape[:-1] +
            (self.nb_heads, self.mid_head_size))  # [B, N, N, H, F]

        pre_att = torch.permute(pre_att, (0, 3, 1, 2, 4))  # [B, H, N, N, F]

        # This part is not very efficient, but we agree to keep it this way to
        # enhance readability, assuming `nb_heads` will not be large.
        logit_heads = []
        for head in range(self.nb_heads):
            logit_heads.append(
                torch.squeeze(self.a_heads[head](F.leaky_relu(pre_att[:, head])),
                              dim=-1))  # [B, N, N]

        logits = torch.stack(logit_heads, dim=1)  # [B, H, N, N]

        coefs = F.softmax(logits + bias_mat, dim=-1)
        ret = torch.matmul(coefs, values)  # [B, H, N, F]
        ret = torch.permute(ret, (0, 2, 1, 3))  # [B, N, H, F]
        ret = torch.reshape(ret, ret.shape[:-2] + (self.hidden_size,))  # [B, N, H*F]

        if self.residual:
            ret += self.skip(z)

        if self.activation is not None:
            ret = self.activation(ret)

        if self.use_ln:
            ret = self.ln(ret)

        return ret, None  # pytype: disable=bad-return-type  # numpy-scalars


class GATv2Full(GATv2):
    """Graph Attention Network v2 with full adjacency matrix."""

    def forward(self, node_fts: _Array, edge_fts: _Array, graph_fts: _Array,
                adj_mat: _Array, hidden: _Array, **unused_kwargs) -> _Array:
        adj_mat = torch.ones_like(adj_mat)
        return super().forward(node_fts, edge_fts, graph_fts, adj_mat, hidden)


class PGN(Processor):
    """Pointer Graph Networks (Veličković et al., NeurIPS 2020)."""

    def __init__(
        self,
        hidden_size: int,
        mid_size: Optional[int] = None,
        mid_act: Optional[_Fn] = None,
        activation: Optional[_Fn] = F.relu,
        reduction: _Fn = torch.max,
        msgs_mlp_sizes: Optional[List[int]] = None,
        use_ln: bool = False,
        use_triplets: bool = False,
        nb_triplet_fts: int = 8,
        gated: bool = False,
    ):
        super().__init__()
        if mid_size is None:
            self.mid_size = hidden_size
        else:
            self.mid_size = mid_size
        self.hidden_size = hidden_size
        self.mid_act = mid_act
        self.activation = activation
        self.reduction = reduction
        self._msgs_mlp_sizes = msgs_mlp_sizes
        self.use_ln = use_ln
        self.use_triplets = use_triplets
        self.nb_triplet_fts = nb_triplet_fts
        self.gated = gated

        self.m_1 = nn.Linear(2 * self.hidden_size, self.mid_size)
        self.m_2 = nn.Linear(2 * self.hidden_size, self.mid_size)
        self.m_e = nn.Linear(self.hidden_size, self.mid_size)
        self.m_g = nn.Linear(self.hidden_size, self.mid_size)

        if self.use_triplets:
            self.t_1 = nn.Linear(2 * self.hidden_size, self.nb_triplet_fts)
            self.t_2 = nn.Linear(2 * self.hidden_size, self.nb_triplet_fts)
            self.t_3 = nn.Linear(2 * self.hidden_size, self.nb_triplet_fts)
            self.t_e_1 = nn.Linear(self.hidden_size, self.nb_triplet_fts)
            self.t_e_2 = nn.Linear(self.hidden_size, self.nb_triplet_fts)
            self.t_e_3 = nn.Linear(self.hidden_size, self.nb_triplet_fts)
            self.t_g = nn.Linear(self.hidden_size, self.nb_triplet_fts)

        self.o1 = nn.Linear(2 * self.hidden_size, self.hidden_size)

        if self._msgs_mlp_sizes is None:
            self.o2 = nn.Linear(self.mid_size, self.hidden_size)
        else:
            self.o2 = nn.Linear(self._msgs_mlp_sizes[-1], self.hidden_size)

        if self.use_triplets:
            self.o3 = nn.Linear(self.nb_triplet_fts, self.hidden_size)

        if self._msgs_mlp_sizes is not None:
            self.mlp = nn.MLP(self.mid_size, self._msgs_mlp_sizes)

        if self.use_ln:
            self.ln = torch.nn.LayerNorm(normalized_shape=self.hidden_size)

        if self.gated:
            self.gate1 = nn.Linear(2 * self.hidden_size, self.hidden_size)
            self.gate2 = nn.Linear(self.mid_size, self.hidden_size)
            self.gate3 = nn.Linear(self.hidden_size, self.hidden_size,
                                   b_init_=functools.partial(
                                       torch.nn.init.constant_,
                                       val=-3))

    def forward(  # pytype: disable=signature-mismatch  # numpy-scalars
        self,
        node_fts: _Array,
        edge_fts: _Array,
        graph_fts: _Array,
        adj_mat: _Array,
        hidden: _Array,
        **unused_kwargs,
    ) -> _Array:
        """MPNN inference step."""

        b, n, _ = node_fts.shape
        assert edge_fts.shape[:-1] == (b, n, n)
        assert graph_fts.shape[:-1] == (b, )
        assert adj_mat.shape == (b, n, n)

        z = torch.concat([node_fts, hidden], dim=-1)

        msg_1 = self.m_1(z)
        msg_2 = self.m_2(z)
        msg_e = self.m_e(edge_fts)
        msg_g = self.m_g(graph_fts)

        tri_msgs = None

        if self.use_triplets:
            # Triplet messages, as done by Dudzik and Velickovic (2022)

            tri_1 = self.t_1(z)
            tri_2 = self.t_2(z)
            tri_3 = self.t_3(z)
            tri_e_1 = self.t_e_1(edge_fts)
            tri_e_2 = self.t_e_2(edge_fts)
            tri_e_3 = self.t_e_3(edge_fts)
            tri_g = self.t_g(graph_fts)

            triplets = (util.expand_dims(tri_1, dims=(2, 3)) +  #   (B, N, 1, 1, H)
                        util.expand_dims(tri_2, dims=(1, 3)) +  # + (B, 1, N, 1, H)
                        util.expand_dims(tri_3, dims=(1, 2)) +  # + (B, 1, 1, N, H)
                        torch.unsqueeze(tri_e_1, dim=3) +  # + (B, N, N, 1, H)
                        torch.unsqueeze(tri_e_2, dim=2) +  # + (B, N, 1, N, H)
                        torch.unsqueeze(tri_e_3, dim=1) +  # + (B, 1, N, N, H)
                        util.expand_dims(tri_g, dims=(1, 2, 3))  # + (B, 1, 1, 1, H)
                        )  # = (B, N, N, N, H)

            tri_msgs = self.o3(torch.max(triplets, dim=1).values)  # (B, N, N, H)

            if self.activation is not None:
                tri_msgs = self.activation(tri_msgs)

        msgs = (torch.unsqueeze(msg_1, dim=1) +
                torch.unsqueeze(msg_2, dim=2) + msg_e +
                util.expand_dims(msg_g, dims=(1, 2)))

        if self._msgs_mlp_sizes is not None:
            msgs = self.mlp(F.relu(msgs))

        if self.mid_act is not None:
            msgs = self.mid_act(msgs)

        if self.reduction == torch.mean:
            msgs = torch.sum(msgs * torch.unsqueeze(adj_mat, -1), dim=1)
            msgs = msgs / torch.sum(adj_mat, dim=-1, keepdims=True)
        elif self.reduction == torch.max:
            maxarg = torch.where(torch.unsqueeze(adj_mat, -1).to(torch.bool),
                                 msgs, -BIG_NUMBER)
            msgs, _ = torch.max(maxarg, dim=1)
        else:
            msgs = self.reduction(msgs * torch.unsqueeze(adj_mat, -1), dim=1)

        h_1 = self.o1(z)
        h_2 = self.o2(msgs)

        ret = h_1 + h_2

        if self.activation is not None:
            ret = self.activation(ret)

        if self.use_ln:
            ret = self.ln(ret)

        if self.gated:
            gate = F.sigmoid(self.gate3(F.relu(self.gate1(z) + self.gate2(msgs))))
            ret = ret * gate + hidden * (1 - gate)

        return ret, tri_msgs  # pytype: disable=bad-return-type  # numpy-scalars


class DeepSets(PGN):
    """Deep Sets (Zaheer et al., NeurIPS 2017)."""

    def forward(self, node_fts: _Array, edge_fts: _Array, graph_fts: _Array,
                adj_mat: _Array, hidden: _Array, **unused_kwargs) -> _Array:
        assert adj_mat.ndim == 3
        adj_mat = torch.ones_like(adj_mat) * torch.eye(adj_mat.shape[-1])
        return super().forward(node_fts, edge_fts, graph_fts, adj_mat, hidden)


class MPNN(PGN):
    """Message-Passing Neural Network (Gilmer et al., ICML 2017)."""

    def forward(self, node_fts: _Array, edge_fts: _Array, graph_fts: _Array,
                adj_mat: _Array, hidden: _Array, **unused_kwargs) -> _Array:
        adj_mat = torch.ones_like(adj_mat)
        return super().forward(node_fts, edge_fts, graph_fts, adj_mat, hidden)


class PGNMask(PGN):
    """Masked Pointer Graph Networks (Veličković et al., NeurIPS 2020)."""

    @property
    def inf_bias(self):
        return True

    @property
    def inf_bias_edge(self):
        return True


class MemNetMasked(Processor):
    """Implementation of End-to-End Memory Networks.

  Inspired by the description in https://arxiv.org/abs/1503.08895.
  """

    def __init__(self,
                 vocab_size: int,
                 sentence_size: int,
                 linear_output_size: int,
                 embedding_size: int = 16,
                 memory_size: Optional[int] = 128,
                 num_hops: int = 1,
                 nonlin: Callable[[Any], Any] = F.relu,
                 apply_embeddings: bool = True,
                 init_func: Callable[[_Array], _Array] = torch.nn.init.zeros_,
                 use_ln: bool = False) -> None:
        """Constructor.

    Args:
      vocab_size: the number of words in the dictionary (each story, query and
        answer come contain symbols coming from this dictionary).
      sentence_size: the dimensionality of each memory.
      linear_output_size: the dimensionality of the output of the last layer
        of the model.
      embedding_size: the dimensionality of the latent space to where all
        memories are projected.
      memory_size: the number of memories provided.
      num_hops: the number of layers in the model.
      nonlin: non-linear transformation applied at the end of each layer.
      apply_embeddings: flag whether to apply embeddings.
      init_func: initialization function for the biases.
      use_ln: whether to use layer normalisation in the model.
    """
        super().__init__()
        self._vocab_size = vocab_size
        self._embedding_size = embedding_size
        self._sentence_size = sentence_size
        self._memory_size = memory_size
        self._linear_output_size = linear_output_size
        self._num_hops = num_hops
        self._nonlin = nonlin
        self._apply_embeddings = apply_embeddings
        self._init_func = init_func
        self._use_ln = use_ln
        # Encoding part: i.e. "I" of the paper.
        self._encodings = _position_encoding(sentence_size, embedding_size)

        if self._apply_embeddings:
            self.query_biases = Parameter(
                self._init_func(
                    torch.empty(
                        size=[self._vocab_size - 1, self._embedding_size])))
            self.stories_biases = Parameter(
                self._init_func(
                    torch.empty(
                        size=[self._vocab_size - 1, self._embedding_size])))
            self.memory_biases = Parameter(
                self._init_func(
                    torch.empty(
                        size=[self._memory_size, self._embedding_size])))
            self.output_biases = Parameter(
                self._init_func(
                    torch.empty(
                        size=[self._vocab_size - 1, self._embedding_size])))

        if self._num_hops > 1:
            if self._apply_embeddings:
                self.intermediate_linear = nn.Linear(self._embedding_size,
                                                     self._embedding_size,
                                                     bias=False)
            else:
                self.intermediate_linear = nn.Linear(self._sentence_size,
                                                     self._embedding_size,
                                                     bias=False)

        # Output_linear is "H".
        self.output_linear = nn.Linear(self._embedding_size,
                                       self._linear_output_size,
                                       bias=False)

        # This linear here is "W".
        self.predict_linear = nn.Linear(self._linear_output_size,
                                        self._vocab_size,
                                        bias=False)

        if self._use_ln:
            self.ln = torch.nn.LayerNorm(normalized_shape=self._vocab_size)

    def forward(  # pytype: disable=signature-mismatch  # numpy-scalars
        self,
        node_fts: _Array,
        edge_fts: _Array,
        graph_fts: _Array,
        adj_mat: _Array,
        hidden: _Array,
        **unused_kwargs,
    ) -> _Array:
        """MemNet inference step."""

        del hidden
        node_and_graph_fts = torch.concat([node_fts, graph_fts[:, None]],
                                          dim=1)
        edge_fts_padded = F.pad(edge_fts * adj_mat[..., None],
                                (0, 0, 0, 1, 0, 1, 0, 0))
        if experimental.use_vmap:
            nxt_hidden = torch.vmap(self._apply, (1), 1)(node_and_graph_fts,
                                                         edge_fts_padded)
        else:
            accum_hiddens = []
            for i in torch.arange(node_and_graph_fts.shape[1]):
                hidden = self._apply(node_and_graph_fts[:, i],
                                     edge_fts_padded[:, i])
                accum_hiddens.append(hidden)
            nxt_hidden = torch.stack(accum_hiddens, dim=1)
        # Broadcast hidden state corresponding to graph features across the nodes.
        nxt_hidden = nxt_hidden[:, :-1] + nxt_hidden[:, -1:]
        return nxt_hidden, None  # pytype: disable=bad-return-type  # numpy-scalars

    def _apply(self, queries: _Array, stories: _Array) -> _Array:
        """Apply Memory Network to the queries and stories.

    Args:
      queries: Tensor of shape [batch_size, sentence_size].
      stories: Tensor of shape [batch_size, memory_size, sentence_size].

    Returns:
      Tensor of shape [batch_size, vocab_size].
    """
        if self._apply_embeddings:
            nil_word_slot = torch.zeros([1, self._embedding_size])

        # This is "A" in the paper.
        if self._apply_embeddings:
            stories_biases = torch.concat([self.stories_biases, nil_word_slot],
                                          dim=0)
            memory_embeddings = stories_biases[
                stories.reshape([-1]).to(torch.int32), ...
            ].reshape(list(stories.shape) + [self._embedding_size])
            memory_embeddings = F.pad(memory_embeddings,
                                      (0, 0, 0, 0, 0, self._memory_size -
                                       memory_embeddings.shape[1], 0, 0))
            memory = torch.sum(memory_embeddings * self._encodings,
                               2) + self.memory_biases
        else:
            memory = stories

        # This is "B" in the paper. Also, when there are no queries (only
        # sentences), then there these lines are substituted by
        # query_embeddings = 0.1.
        if self._apply_embeddings:
            query_biases = torch.concat([self.query_biases, nil_word_slot],
                                        dim=0)
            query_embeddings = query_biases[
                queries.reshape([-1]).to(torch.int32), ...
            ].reshape(list(queries.shape) + [self._embedding_size])
            # This is "u" in the paper.
            query_input_embedding = torch.sum(
                query_embeddings * self._encodings, 1)
        else:
            query_input_embedding = queries

        # This is "C" in the paper.
        if self._apply_embeddings:
            output_biases = torch.concat([self.output_biases, nil_word_slot],
                                         dim=0)
            output_embeddings = output_biases[
                stories.reshape([-1]).to(torch.int32), ...
            ].reshape(list(stories.shape) + [self._embedding_size])
            output_embeddings = F.pad(output_embeddings,
                                      (0, 0, 0, 0, 0, self._memory_size -
                                       output_embeddings.shape[1], 0, 0))
            output = torch.sum(output_embeddings * self._encodings, 2)
        else:
            output = stories

        for hop_number in range(self._num_hops):
            query_input_embedding_transposed = torch.permute(
                torch.unsqueeze(query_input_embedding, -1), [0, 2, 1])

            # Calculate probabilities.
            probs = F.softmax(
                torch.sum(memory * query_input_embedding_transposed, 2), -1)

            # Calculate output of the layer by multiplying by C.
            transposed_probs = torch.permute(torch.unsqueeze(probs, -1),
                                             [0, 2, 1])
            transposed_output_embeddings = torch.permute(output, [0, 2, 1])

            # This is "o" in the paper.
            layer_output = torch.sum(
                transposed_output_embeddings * transposed_probs, 2)

            # Finally the answer
            if hop_number == self._num_hops - 1:
                # Please note that in the TF version we apply the final linear layer
                # in all hops and this results in shape mismatches.
                output_layer = self.output_linear(query_input_embedding +
                                                  layer_output)
            else:
                output_layer = self.intermediate_linear(query_input_embedding +
                                                        layer_output)

            query_input_embedding = output_layer
            if self._nonlin:
                output_layer = self._nonlin(output_layer)

        # This linear here is "W".
        ret = self.predict_linear(output_layer)

        if self._use_ln:
            ret = self.ln(ret)

        return ret


class MemNetFull(MemNetMasked):
    """Memory Networks with full adjacency matrix."""

    def forward(self, node_fts: _Array, edge_fts: _Array, graph_fts: _Array,
                adj_mat: _Array, hidden: _Array, **unused_kwargs) -> _Array:
        adj_mat = torch.ones_like(adj_mat)
        return super().forward(node_fts, edge_fts, graph_fts, adj_mat, hidden)


ProcessorFactory = Callable[[int], Processor]


def get_processor_factory(kind: str,
                          use_ln: bool,
                          nb_triplet_fts: int,
                          nb_heads: Optional[int] = None) -> ProcessorFactory:
    """Returns a processor factory.

  Args:
    kind: One of the available types of processor.
    use_ln: Whether the processor passes the output through a layernorm layer.
    nb_triplet_fts: How many triplet features to compute.
    nb_heads: Number of attention heads for GAT processors.
  Returns:
    A callable that takes an `out_size` parameter (equal to the hidden
    dimension of the network) and returns a processor instance.
  """

    def _factory(hidden_size: int):
        if kind == 'deepsets':
            processor = DeepSets(hidden_size=hidden_size,
                                 msgs_mlp_sizes=[hidden_size, hidden_size],
                                 use_ln=use_ln,
                                 use_triplets=False,
                                 nb_triplet_fts=0)
        elif kind == 'gat':
            processor = GAT(
                hidden_size=hidden_size,
                nb_heads=nb_heads,
                use_ln=use_ln,
            )
        elif kind == 'gat_full':
            processor = GATFull(hidden_size=hidden_size,
                                nb_heads=nb_heads,
                                use_ln=use_ln)
        elif kind == 'gatv2':
            processor = GATv2(hidden_size=hidden_size,
                              nb_heads=nb_heads,
                              use_ln=use_ln)
        elif kind == 'gatv2_full':
            processor = GATv2Full(hidden_size=hidden_size,
                                  nb_heads=nb_heads,
                                  use_ln=use_ln)
        elif kind == 'memnet_full':
            processor = MemNetFull(
                vocab_size=hidden_size,
                sentence_size=hidden_size,
                linear_output_size=hidden_size,
            )
        elif kind == 'memnet_masked':
            processor = MemNetMasked(
                vocab_size=hidden_size,
                sentence_size=hidden_size,
                linear_output_size=hidden_size,
            )
        elif kind == 'mpnn':
            processor = MPNN(
                hidden_size=hidden_size,
                msgs_mlp_sizes=[hidden_size, hidden_size],
                use_ln=use_ln,
                use_triplets=False,
                nb_triplet_fts=0,
            )
        elif kind == 'pgn':
            processor = PGN(
                hidden_size=hidden_size,
                msgs_mlp_sizes=[hidden_size, hidden_size],
                use_ln=use_ln,
                use_triplets=False,
                nb_triplet_fts=0,
            )
        elif kind == 'pgn_mask':
            processor = PGNMask(
                hidden_size=hidden_size,
                msgs_mlp_sizes=[hidden_size, hidden_size],
                use_ln=use_ln,
                use_triplets=False,
                nb_triplet_fts=0,
            )
        elif kind == 'triplet_mpnn':
            processor = MPNN(
                hidden_size=hidden_size,
                msgs_mlp_sizes=[hidden_size, hidden_size],
                use_ln=use_ln,
                use_triplets=True,
                nb_triplet_fts=nb_triplet_fts,
            )
        elif kind == 'triplet_pgn':
            processor = PGN(
                hidden_size=hidden_size,
                msgs_mlp_sizes=[hidden_size, hidden_size],
                use_ln=use_ln,
                use_triplets=True,
                nb_triplet_fts=nb_triplet_fts,
            )
        elif kind == 'triplet_pgn_mask':
            processor = PGNMask(
                hidden_size=hidden_size,
                msgs_mlp_sizes=[hidden_size, hidden_size],
                use_ln=use_ln,
                use_triplets=True,
                nb_triplet_fts=nb_triplet_fts,
            )
        elif kind == 'gpgn':
            processor = PGN(
                hidden_size=hidden_size,
                msgs_mlp_sizes=[hidden_size, hidden_size],
                use_ln=use_ln,
                use_triplets=False,
                nb_triplet_fts=nb_triplet_fts,
                gated=True,
            )
        elif kind == 'gpgn_mask':
            processor = PGNMask(
                hidden_size=hidden_size,
                msgs_mlp_sizes=[hidden_size, hidden_size],
                use_ln=use_ln,
                use_triplets=False,
                nb_triplet_fts=nb_triplet_fts,
                gated=True,
            )
        elif kind == 'gmpnn':
            processor = MPNN(
                hidden_size=hidden_size,
                msgs_mlp_sizes=[hidden_size, hidden_size],
                use_ln=use_ln,
                use_triplets=False,
                nb_triplet_fts=nb_triplet_fts,
                gated=True,
            )
        elif kind == 'triplet_gpgn':
            processor = PGN(
                hidden_size=hidden_size,
                msgs_mlp_sizes=[hidden_size, hidden_size],
                use_ln=use_ln,
                use_triplets=True,
                nb_triplet_fts=nb_triplet_fts,
                gated=True,
            )
        elif kind == 'triplet_gpgn_mask':
            processor = PGNMask(
                hidden_size=hidden_size,
                msgs_mlp_sizes=[hidden_size, hidden_size],
                use_ln=use_ln,
                use_triplets=True,
                nb_triplet_fts=nb_triplet_fts,
                gated=True,
            )
        elif kind == 'triplet_gmpnn':
            processor = MPNN(
                hidden_size=hidden_size,
                msgs_mlp_sizes=[hidden_size, hidden_size],
                use_ln=use_ln,
                use_triplets=True,
                nb_triplet_fts=nb_triplet_fts,
                gated=True,
            )
        else:
            raise ValueError('Unexpected processor kind ' + kind)

        return processor

    return _factory


def _position_encoding(sentence_size: int, embedding_size: int) -> _Array:
    """Position Encoding described in section 4.1 [1]."""
    encoding = torch.ones((embedding_size, sentence_size), dtype=torch.float32)
    ls = sentence_size + 1
    le = embedding_size + 1
    for i in range(1, le):
        for j in range(1, ls):
            encoding[i - 1, j - 1] = (i - (le - 1) / 2) * (j - (ls - 1) / 2)
    encoding = 1 + 4 * encoding / embedding_size / sentence_size
    return torch.t(encoding)
