# Copyright 2023 Samuele Bodrini
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Sampling utilities."""

from typing import List

import torch
from clrs._src import samplers as clrs_samplers

from clrs_torch._src import probing
from clrs_torch._src.probing import torchify_datapoint


_Array = torch.Tensor
_DataPoint = probing.DataPoint
_Features = clrs_samplers.Features
_FeaturesChunked = clrs_samplers.FeaturesChunked
_Feedback = clrs_samplers.Feedback
Trajectory = List[_DataPoint]
Trajectories = List[Trajectory]


def torchify_feedback(feedback):
    inputs = tuple(map(torchify_datapoint, feedback.features.inputs))
    hints = tuple(map(torchify_datapoint, feedback.features.hints))
    outputs = list(map(torchify_datapoint, feedback.outputs))

    if isinstance(feedback.features, _FeaturesChunked):
        is_first = torch.tensor(feedback.features.is_first, dtype=torch.bool)
        is_last = torch.tensor(feedback.features.is_last, dtype=torch.bool)
        features = _FeaturesChunked(inputs=inputs,
                                    hints=hints,
                                    is_first=is_first,
                                    is_last=is_last)
    else:
        lengths = torch.tensor(feedback.features.lengths, dtype=torch.float32)
        features = _Features(inputs=inputs,
                             hints=hints,
                             lengths=lengths)

    return _Feedback(features=features, outputs=outputs)


def torchify_sampler(sample_iterator):
    """Converts samples' ndarrays to tensors.

  Args:
    sample_iterator: An iterator producing samples with data encoded as a numpy.ndarray.
  Returns:
    An iterator returning the samples with data encoded as a torch.Tensor.
  """

    def _iterate():
        while True:
            feedback = next(sample_iterator)
            yield torchify_feedback(feedback)

    return _iterate()
