# Copyright 2021 DeepMind Technologies Limited. All Rights Reserved.
# Modifications copyright 2023 Samuele Boldrini
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Tests for processors.py."""

import torch
from absl.testing import absltest

from clrs_torch._src import processors


class MemnetTest(absltest.TestCase):

    def test_simple_run_and_check_shapes(self):

        batch_size = 64
        vocab_size = 177
        embedding_size = 64
        sentence_size = 11
        memory_size = 320
        linear_output_size = 128
        num_hops = 2
        use_ln = True

        queries = torch.ones([batch_size, sentence_size], dtype=torch.int32)
        stories = torch.ones([batch_size, memory_size, sentence_size],
                             dtype=torch.int32)

        torch.random.manual_seed(42)
        model = processors.MemNetFull(
            vocab_size=vocab_size,
            embedding_size=embedding_size,
            sentence_size=sentence_size,
            memory_size=memory_size,
            linear_output_size=linear_output_size,
            num_hops=num_hops,
            use_ln=use_ln
        )

        model_output = model._apply(queries, stories)
        assert model_output.shape == torch.Size([batch_size, vocab_size])
        assert model_output.dtype == torch.float32


if __name__ == '__main__':
    absltest.main()
