# Copyright 2021 DeepMind Technologies Limited. All Rights Reserved.
# Modifications copyright 2023 Samuele Boldrini
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Torch implementation of CLRS baseline models."""

import os
from typing import Dict, List, Optional, Tuple, Union

import jax
import torch
import torch.nn as nn
from clrs._src import model, specs

import clrs_torch._src.util as util
from clrs_torch._src import decoders, losses, nets, probing, processors, samplers


_Array = torch.Tensor
_DataPoint = probing.DataPoint
_Features = samplers._Features
_FeaturesChunked = samplers._FeaturesChunked
_Feedback = samplers._Feedback
_Location = specs.Location
_Seed = torch.int64
_Spec = specs.Spec
_Stage = specs.Stage
_Trajectory = samplers.Trajectory
_Type = specs.Type
_OutputClass = specs.OutputClass


class BaselineModel(model.Model):
    """Model implementation with selectable message passing algorithm."""

    def __init__(
        self,
        spec: Union[_Spec, List[_Spec]],
        dummy_trajectory: Union[List[_Feedback], _Feedback],
        processor_factory: processors.ProcessorFactory,
        features_list: List[List[_FeaturesChunked]] = None,
        hidden_dim: int = 32,
        encode_hints: bool = False,
        decode_hints: bool = True,
        encoder_init: str = 'default',
        use_lstm: bool = False,
        learning_rate: float = 0.005,
        grad_clip_max_norm: float = 0.0,
        checkpoint_path: str = '/tmp/clrs3',
        freeze_processor: bool = False,
        dropout_prob: float = 0.0,
        hint_teacher_forcing: float = 0.0,
        hint_repred_mode: str = 'soft',
        nb_msg_passing_steps: int = 1,
    ):
        """Constructor for BaselineModel.

    The model consists of encoders, processor and decoders. It can train
    and evaluate either a single algorithm or a set of algorithms; in the
    latter case, a single processor is shared among all the algorithms, while
    the encoders and decoders are separate for each algorithm.

    Args:
      spec: Either a single spec for one algorithm, or a list of specs for
        multiple algorithms to be trained and evaluated.
      dummy_trajectory: Either a single feedback batch, in the single-algorithm
        case, or a list of feedback batches, in the multi-algorithm case, that
        comply with the `spec` (or list of specs), to initialize network size.
      processor_factory: A callable that takes an `out_size` parameter
        and returns a processor (see `processors.py`).
      features_list: A list of datapoint of all length for all algorithms,
        used to initialize the message passing state. This is only useful
        for BaselineModelChunked and ingored by BaselineModel.
      hidden_dim: Size of the hidden state of the model, i.e., size of the
        message-passing vectors.
      encode_hints: Whether to provide hints as model inputs.
      decode_hints: Whether to provide hints as model outputs.
      encoder_init: The initialiser type to use for the encoders.
      use_lstm: Whether to insert an LSTM after message passing.
      learning_rate: Learning rate for training.
      grad_clip_max_norm: if greater than 0, the maximum norm of the gradients.
      checkpoint_path: Path for loading/saving checkpoints.
      freeze_processor: If True, the processor weights will be frozen and
        only encoders and decoders (and, if used, the lstm) will be trained.
      dropout_prob: Dropout rate in the message-passing stage.
      hint_teacher_forcing: Probability of using ground-truth hints instead
        of predicted hints as inputs during training (only relevant if
        `encode_hints`=True)
      hint_repred_mode: How to process predicted hints when fed back as inputs.
        Only meaningful when `encode_hints` and `decode_hints` are True.
        Options are:
          - 'soft', where we use softmaxes for categoricals, pointers
              and mask_one, and sigmoids for masks. This will allow gradients
              to flow through hints during training.
          - 'hard', where we use argmax instead of softmax, and hard
              thresholding of masks. No gradients will go through the hints
              during training; even for scalar hints, which don't have any
              kind of post-processing, gradients will be stopped.
          - 'hard_on_eval', which is soft for training and hard for evaluation.
      nb_msg_passing_steps: Number of message passing steps per hint.

    Raises:
      ValueError: if `encode_hints=True` and `decode_hints=False`.
    """
        super(BaselineModel, self).__init__(spec=spec)

        if encode_hints and not decode_hints:
            raise ValueError(
                '`encode_hints=True`, `decode_hints=False` is invalid.')

        assert hint_repred_mode in ['soft', 'hard', 'hard_on_eval']

        self.decode_hints = decode_hints
        self.checkpoint_path = checkpoint_path
        self.freeze_processor = freeze_processor
        self.grad_clip_max_norm = grad_clip_max_norm

        self.nb_msg_passing_steps = nb_msg_passing_steps

        self.nb_dims = []
        if isinstance(dummy_trajectory, _Feedback):
            assert len(self._spec) == 1
            dummy_trajectory = [dummy_trajectory]
        for traj in dummy_trajectory:
            nb_dims = {}
            for inp in traj.features.inputs:
                nb_dims[inp.name] = inp.data.shape[-1]
            for hint in traj.features.hints:
                nb_dims[hint.name] = hint.data.shape[-1]
            for outp in traj.outputs:
                nb_dims[outp.name] = outp.data.shape[-1]
            self.nb_dims.append(nb_dims)

        self._create_net_fns(features_list, hidden_dim, encode_hints,
                             processor_factory, use_lstm, encoder_init,
                             dropout_prob, hint_teacher_forcing, hint_repred_mode)
        # Conditionally freeze processor
        self.net.processor.requires_grad_(not self.freeze_processor)

        self.opt = torch.optim.Adam(self.net.parameters(), lr=learning_rate)

    def _create_net_fns(self, features_list, hidden_dim, encode_hints,
                        processor_factory, use_lstm, encoder_init,
                        dropout_prob, hint_teacher_forcing, hint_repred_mode):

        disable_compilation = True

        self.net = nets.Net(self._spec, hidden_dim, encode_hints,
                            self.decode_hints, processor_factory, use_lstm,
                            encoder_init, dropout_prob, hint_teacher_forcing,
                            hint_repred_mode, self.nb_dims,
                            self.nb_msg_passing_steps)
        self.compiled_grad = torch.compile(self._compute_grad, disable=disable_compilation)
        self.compiled_feedback = torch.compile(self._feedback, disable=disable_compilation)
        self.compiled_predict = torch.compile(self._predict, disable=disable_compilation)
        self.compiled_accum_opt_update = torch.compile(self._accum_opt_update, disable=disable_compilation)

    def _compute_grad(self, feedback, algorithm_index):
        self.opt.zero_grad(set_to_none=True)
        lss = self._loss(feedback, algorithm_index)
        lss.backward()
        grads = jax.tree_util.tree_map(lambda param: param.grad,
                                       dict(self.net.named_parameters()))
        return lss, grads

    def _feedback(self, feedback, algorithm_index):
        self.opt.zero_grad(set_to_none=True)
        lss = self._loss(feedback, algorithm_index)
        lss.backward()
        if self.grad_clip_max_norm != 0.0:
            nn.utils.clip_grad_norm_(self.net.parameters(), self.grad_clip_max_norm)
        self.opt.step()
        return lss

    def _predict(self, features: _Features,
                 algorithm_index: int, return_hints: bool,
                 return_all_outputs: bool):
        outs, hint_preds = self.net(
            features,
            repred=True,
            algorithm_index=algorithm_index,
            return_hints=return_hints,
            return_all_outputs=return_all_outputs)
        outs = decoders.postprocess(
            self._spec[algorithm_index],
            outs,
            sinkhorn_temperature=0.1,
            sinkhorn_steps=50,
            hard=True,
        )
        return outs, hint_preds

    def compute_grad(
        self,
        feedback: _Feedback,
        algorithm_index: Optional[int] = None,
    ) -> Tuple[_Array, _Array]:
        """Compute gradients."""

        if algorithm_index is None:
            assert len(self._spec) == 1
            algorithm_index = 0
        assert algorithm_index >= 0

        # Calculate gradients.
        loss, grads = self.compiled_grad(feedback, algorithm_index)
        return loss.item(), grads

    def feedback(self,
                 feedback: _Feedback,
                 algorithm_index=None) -> float:
        if algorithm_index is None:
            assert len(self._spec) == 1
            algorithm_index = 0
        # Calculate and apply gradients.
        loss = self.compiled_feedback(feedback, algorithm_index)
        return loss.item()

    @torch.inference_mode()
    def predict(self,
                features: _Features,
                algorithm_index: Optional[int] = None,
                return_hints: bool = False,
                return_all_outputs: bool = False):
        """Model inference step."""
        if algorithm_index is None:
            assert len(self._spec) == 1
            algorithm_index = 0

        return self.compiled_predict(features, algorithm_index,
                                     return_hints, return_all_outputs)

    def _loss(self, feedback, algorithm_index):
        """Calculates model loss f(feedback; params)."""
        output_preds, hint_preds = self.net(
            feedback.features,
            repred=False,
            algorithm_index=algorithm_index,
            return_hints=True,
            return_all_outputs=False)

        nb_nodes = _nb_nodes(feedback, is_chunked=False)
        lengths = feedback.features.lengths
        total_loss = torch.tensor(0.0)

        # Calculate output loss.
        for truth in feedback.outputs:
            total_loss += losses.output_loss(
                truth=truth,
                pred=output_preds[truth.name],
                nb_nodes=nb_nodes,
            )

        # Optionally accumulate hint losses.
        if self.decode_hints:
            for truth in feedback.features.hints:
                total_loss += losses.hint_loss(
                    truth=truth,
                    preds=[x[truth.name] for x in hint_preds],
                    lengths=lengths,
                    nb_nodes=nb_nodes,
                )
        return total_loss

    def _accum_opt_update(self, grads):
        """Update params from gradients collected from several algorithms."""
        # Replace all None gradients with 0
        grads = [jax.tree_util.tree_map(lambda x: torch.zeros(1) if x is None else x,
                                        grad, is_leaf=lambda x: x is None)
                 for grad in grads]
        # Average the gradients over all algos
        grads = jax.tree_util.tree_map(
            lambda *x: sum(x) / (sum([torch.any(k) for k in x]) + 1e-12), *grads)

        for k, v in self.net.named_parameters():
            if v.grad is not None:
                v.grad = grads[k]

        if self.grad_clip_max_norm != 0.0:
            nn.utils.clip_grad_norm_(self.net.parameters(), self.grad_clip_max_norm)
        self.opt.step()

    @torch.inference_mode()
    def update_model_params_accum(self, grads) -> None:
        self.compiled_accum_opt_update(grads)

    def verbose_loss(self, feedback: _Feedback,
                     extra_info) -> Dict[str, _Array]:
        """Gets verbose loss information."""
        hint_preds = extra_info

        nb_nodes = _nb_nodes(feedback, is_chunked=False)
        lengths = feedback.features.lengths
        losses_ = {}

        # Optionally accumulate hint losses.
        if self.decode_hints:
            for truth in feedback.features.hints:
                losses_.update(
                    losses.hint_loss(
                        truth=truth,
                        preds=[x[truth.name] for x in hint_preds],
                        lengths=lengths,
                        nb_nodes=nb_nodes,
                        verbose=True,
                    ))

        return losses_

    def restore_model(self, file_name: str, only_load_processor: bool = False):
        """Restore model from `file_name`."""
        path = os.path.join(self.checkpoint_path, file_name)
        with open(path, 'rb') as f:
            restored_state = torch.load(f)
            if only_load_processor:
                self.net.processor.load_state_dict(_filter_in_processor(restored_state))
            else:
                self.net.load_state_dict(restored_state)

    def save_model(self, file_name: str):
        """Save model (processor weights only) to `file_name`."""
        os.makedirs(self.checkpoint_path, exist_ok=True)
        to_save = self.net.state_dict()
        path = os.path.join(self.checkpoint_path, file_name)
        with open(path, 'wb') as f:
            torch.save(to_save, f)


class BaselineModelChunked(BaselineModel):
    """Model that processes time-chunked data.

    Unlike `BaselineModel`, which processes full samples, `BaselineModelChunked`
    processes fixed-timelength chunks of data. Each tensor of inputs and hints
    has dimensions chunk_length x batch_size x ... The beginning of a new
    sample withing the chunk is signalled by a tensor called `is_first` of
    dimensions chunk_length x batch_size.

    The chunked model is intended for training. For validation and test, use
    `BaselineModel`.
  """

    mp_states: List[List[nets.MessagePassingStateChunked]]
    init_mp_states: List[List[nets.MessagePassingStateChunked]]

    def _create_net_fns(self, features_list, hidden_dim, encode_hints,
                        processor_factory, use_lstm, encoder_init,
                        dropout_prob, hint_teacher_forcing, hint_repred_mode):
        assert features_list is not None
        disable_compilation = True

        self.net = nets.NetChunked(self._spec, hidden_dim, encode_hints,
                                   self.decode_hints, processor_factory,
                                   use_lstm, encoder_init, dropout_prob,
                                   hint_teacher_forcing, hint_repred_mode,
                                   self.nb_dims,
                                   self.nb_msg_passing_steps)

        self.compiled_grad = torch.compile(self._compute_grad, disable=disable_compilation)
        self.compiled_feedback = torch.compile(self._feedback, disable=disable_compilation)
        self.compiled_predict = torch.compile(self._predict, disable=disable_compilation)
        self.compiled_accum_opt_update = torch.compile(self._accum_opt_update, disable=disable_compilation)

        self.mp_states = self._init_mp_state(features_list)
        self.init_mp_states = [list(x) for x in self.mp_states]

    @torch.inference_mode()
    def _init_mp_state(self, features_list: List[List[_FeaturesChunked]]):
        def _empty_mp_state():
            return nets.MessagePassingStateChunked(  # pytype: disable=wrong-arg-types  # numpy-scalars
                inputs=None,
                hints=None,
                is_first=None,
                hint_preds=None,
                hiddens=None,
                lstm_state=None)

        empty_mp_states = [[_empty_mp_state() for _ in f]
                           for f in features_list]
        mp_states = [
            self.net(f,
                     e,
                     False,
                     init_mp_state=True,
                     algorithm_index=-1)[1]
            for (f, e) in zip(features_list, empty_mp_states)
        ]
        return mp_states

    def predict(self,
                features: _FeaturesChunked,
                algorithm_index: Optional[int] = None):
        """Inference not implemented. Chunked model intended for training only."""
        raise NotImplementedError

    def _loss(self, feedback, mp_state, algorithm_index):
        (output_preds, hint_preds), mp_state = self.net(
            [feedback.features], [mp_state],
            repred=False,
            init_mp_state=False,
            algorithm_index=algorithm_index)

        nb_nodes = _nb_nodes(feedback, is_chunked=True)

        total_loss = torch.tensor(0.0)
        is_first = feedback.features.is_first
        is_last = feedback.features.is_last

        # Calculate output loss.
        for truth in feedback.outputs:
            total_loss += losses.output_loss_chunked(
                truth=truth,
                pred=output_preds[truth.name],
                is_last=is_last,
                nb_nodes=nb_nodes,
            )

        # Optionally accumulate hint losses.
        if self.decode_hints:
            for truth in feedback.features.hints:
                loss = losses.hint_loss_chunked(
                    truth=truth,
                    pred=hint_preds[truth.name],
                    is_first=is_first,
                    nb_nodes=nb_nodes,
                )
                total_loss += loss

        return total_loss, mp_state

    def _compute_grad(self, feedback, mp_state, algorithm_index):
        self.opt.zero_grad(set_to_none=False)
        (lss, mp_state) = self._loss(feedback, mp_state, algorithm_index)
        lss.backward()
        grads = jax.tree_util.tree_map(lambda param: param.grad,
                                       dict(self.net.named_parameters()))
        return lss, mp_state, grads

    def _feedback(self, feedback, mp_state, algorithm_index):
        self.opt.zero_grad(set_to_none=True)
        (lss, mp_state) = self._loss(feedback, mp_state, algorithm_index)
        lss.backward()
        if self.grad_clip_max_norm != 0.0:
            nn.utils.clip_grad_norm_(self.net.parameters(), self.grad_clip_max_norm)
        self.opt.step()
        return lss, mp_state

    def compute_grad(
        self,
        feedback: _Feedback,
        algorithm_index: Optional[Tuple[int, int]] = None,
    ) -> Tuple[float, _Array]:
        """Compute gradients."""

        if algorithm_index is None:
            assert len(self._spec) == 1
            algorithm_index = (0, 0)
        length_index, algorithm_index = algorithm_index
        # Reusing init_mp_state improves performance.
        # The next, commented out line, should be used for proper state keeping.
        # mp_state = self.mp_states[length_index][algorithm_index]
        mp_state = self.init_mp_states[length_index][algorithm_index]
        loss, mp_state, grads = self.compiled_grad(feedback, mp_state,
                                                   algorithm_index)
        self.mp_states[length_index][algorithm_index] = mp_state
        return loss.item(), grads

    def feedback(self,
                 feedback: _Feedback,
                 algorithm_index=None) -> float:
        if algorithm_index is None:
            assert len(self._spec) == 1
            algorithm_index = (0, 0)
        length_index, algorithm_index = algorithm_index
        # Reusing init_mp_state improves performance.
        # The next, commented out line, should be used for proper state keeping.
        # mp_state = self.mp_states[length_index][algorithm_index]
        mp_state = self.init_mp_states[length_index][algorithm_index]
        loss, mp_state = self.compiled_feedback(feedback, mp_state, algorithm_index)
        self.mp_states[length_index][algorithm_index] = mp_state
        return loss

    def verbose_loss(self, *args, **kwargs):
        raise NotImplementedError


def _nb_nodes(feedback: _Feedback, is_chunked) -> int:
    for inp in feedback.features.inputs:
        if inp.location in [_Location.NODE, _Location.EDGE]:
            if is_chunked:
                return inp.data.shape[2]  # inputs are time x batch x nodes x ...
            else:
                return inp.data.shape[1]  # inputs are batch x nodes x ...
    assert False


def _filter_in_processor(params: Dict[str, torch.Tensor]) -> Dict[str, _Array]:
    return {k.removeprefix('processor.'): v
            for k, v in params.items() if k.startswith('processor')}


def _is_not_done_broadcast(lengths, i, tensor):
    is_not_done = (lengths > i + 1) * 1.0
    while len(is_not_done.shape) < len(tensor.shape):
        is_not_done = util.expand_dims(is_not_done, -1)
    return is_not_done
