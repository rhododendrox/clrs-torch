# Copyright 2023 Samuele Boldrini
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import functools
from typing import Callable, Iterable, Optional, Tuple, NamedTuple

import numpy as np
import torch
import torch.nn as nn

from torch.nn import functional as F

Activation = Callable[[torch.Tensor], torch.Tensor]
Initializer = Callable[[torch.Tensor], torch.Tensor]


class Linear(nn.Linear):

    def __init__(
            self,
            in_features: int,
            out_features: int,
            bias: bool = True,
            w_init_: Optional[Initializer] = None,
            b_init_: Optional[Initializer] = nn.init.zeros_,
            device=None,
            dtype=None
    ):
        super().__init__(
            in_features=in_features,
            out_features=out_features,
            bias=bias,
            device=device,
            dtype=dtype
        )

        if w_init_ is None:
            stdenv = 1. / np.sqrt(self.in_features)
            w_init_ = functools.partial(nn.init.trunc_normal_, std=stdenv)

        w_init_(self.weight)
        if bias:
            b_init_(self.bias)


class MLP(nn.Module):

    def __init__(
        self,
        input_dim: int,
        hidden_dims: Iterable[int],
        w_init_: Optional[Initializer] = None,
        b_init_: Optional[Initializer] = nn.init.zeros_,
        bias: bool = True,
        activation: Activation = nn.ReLU(),
        activate_last: bool = False,
        dropout_rate: Optional[float] = None,
    ):
        super().__init__()

        self.layers = nn.ModuleList()
        self.activation = activation
        self.activate_last = activate_last
        self.dropout = None

        in_dim = input_dim
        for hidden_dim in hidden_dims:
            linear = Linear(
                in_dim,
                hidden_dim,
                bias=bias,
                w_init_=w_init_,
                b_init_=b_init_
            )

            self.layers.append(linear)
            in_dim = hidden_dim

        if dropout_rate is not None:
            self.dropout = nn.Dropout(dropout_rate)

    def forward(self, input):
        n_layers = len(self.layers)

        out = input
        for i, layer in enumerate(self.layers):
            out = layer(out)
            if i < (n_layers - 1) or self.activate_last:
                if self.dropout is not None:
                    out = self.dropout(out)
                out = self.activation(out)

        return out


class LSTMState(NamedTuple):
    """An LSTM core state consists of hidden and cell vectors.

  Attributes:
    hidden: Hidden state.
    cell: Cell state.
  """
    hidden: torch.Tensor
    cell: torch.Tensor


class LSTM(nn.Module):

    def __init__(self, hidden_size: int):
        """Constructs an LSTM.

        Args:
        hidden_size: Hidden layer size.
        name: Name of the module.
        """
        super().__init__()
        self.hidden_size = hidden_size
        self.gate = Linear(2 * self.hidden_size, 4 * self.hidden_size)

    def __call__(
        self,
        inputs: torch.Tensor,
        prev_state: LSTMState,
    ) -> Tuple[torch.Tensor, LSTMState]:
        if len(inputs.shape) > 2 or not inputs.shape:
            raise ValueError("LSTM input must be rank-1 or rank-2.")
        x_and_h = torch.concat([inputs, prev_state.hidden], dim=-1)
        gated = self.gate(x_and_h)
        i, g, f, o = torch.split(gated, split_size_or_sections=[self.hidden_size] * 4, dim=-1)
        f = F.sigmoid(f + 1)  # Forget bias as in sonnet.
        c = f * prev_state.cell + F.sigmoid(i) * F.tanh(g)
        h = F.sigmoid(o) * F.tanh(c)
        return h, LSTMState(h, c)

    def initial_state(self, batch_size: Optional[int]) -> LSTMState:
        size = self.hidden_size
        if batch_size is not None:
            size = (batch_size, size)

        state = LSTMState(hidden=torch.zeros(size), cell=torch.zeros(size))

        return state
